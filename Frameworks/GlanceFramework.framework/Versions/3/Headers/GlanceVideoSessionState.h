//
//  GlanceVideoSessionState.h
//  GlanceFramework
//
//  Created by Ankit Desai on 10/4/19.
//  Copyright © 2019 Glance. All rights reserved.
//

typedef enum GlanceVideoSessionState : NSUInteger {
    WAITING_FOR_DEVICE = 0,
    WAITING_FOR_DIMENSIONS = 1,
    WAITING_FOR_CODER_PARAMETERS = 2,
    RESETTING = 3,
    RESTARTING = 4,
    STARTING = 5,
    STARTED = 6,
    STOPPING = 7,
    UPDATING_DIMENSIONS = 8,
    UPDATED_DIMENSIONS = 9,
    ENDING = 10,
    PAUSED = 11,
    FRAME_DROPPED = 12,
    ROTATION = 13
} GlanceVideoSessionState;
