//
//  VideoEncoder.h
//  DeviceVideo
//
//  Created by Ilya Belenkiy on 10/24/17.
//  Copyright © 2017 Glance. All rights reserved.
//

#ifndef VideoEncoder_h
#define VideoEncoder_h

#import <AVFoundation/AVFoundation.h>

@class VideoEncoder;

@protocol VideoEncoderDelegate <NSObject>
- (void)videoEncoderReady:(VideoEncoder* _Nonnull)encoder;
- (void)videoEncoderDidStop:(VideoEncoder* _Nonnull)encoder;
- (void)videoEncoderDidEncodeFirstIFrame:(VideoEncoder* _Nonnull)encoder;
- (void)videoEncoder:(VideoEncoder* _Nonnull)encoder didEncodeSps:( NSData* _Nonnull )sps pps:(NSData* _Nonnull)pps;
- (void)videoEncoder:(VideoEncoder* _Nonnull)encoder didEncodeVideoData:(NSData* _Nonnull)data;
- (void)videoEncoderDidCompressH264:(VideoEncoder* _Nonnull)encoder;
@end

@interface VideoEncoder : NSObject
@property (assign, nonatomic) id<VideoEncoderDelegate> _Nullable delegate;
- (void)reset:(NSUInteger)width height:(NSUInteger)height pframes:(NSUInteger)pframes bitrate:(NSUInteger)bitrate framerate:(NSUInteger)framerate;
- (nullable instancetype)initWithDelegate:(id<VideoEncoderDelegate> _Nonnull) delegate;

- (void) encodeImageBuffer:(_Nonnull CVImageBufferRef)imageBuffer pts:(CMTime)pts duration:(CMTime)duration;
-(void)stop;
@end

#endif /* VideoEncoder_h */

