//
//  AppDelegate.swift
//  Demo app for Host Session (authenticated Glance user)
//  Screenshare and HD Video
//
//  Copyright © 2019 Glance Networks. All rights reserved.
//

import Cocoa
import GlanceFramework

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, GlanceVideoSessionDelegate {
    
    @IBOutlet weak var loginView: NSView!
    @IBOutlet weak var username: NSTextField!
    @IBOutlet weak var password: NSSecureTextField!
    @IBOutlet weak var loginButton: NSButton!
    
    @IBOutlet weak var buttonView: NSView!
    @IBOutlet weak var showMainDisplayButton: NSButton!
    @IBOutlet weak var showSafariButton: NSButton!
    @IBOutlet weak var showThisAppButton: NSButton!
    @IBOutlet weak var endSessionButton : NSButton!
    
    @IBOutlet weak var showIPhoneButton: NSButton!
    @IBOutlet weak var showHDButton: NSButton!
    @IBOutlet weak var showBlackmagicButton: NSButton!
    @IBOutlet weak var endVideoButton: NSButton!
    
    @IBOutlet weak var sessionKeyLabel : NSTextField!
    @IBOutlet weak var statusText: NSTextField!
    
    var settings : GlanceSettings!
    var user : GlanceUser!
    var screenshareSession : GlanceHostSession!
    var videoSession : GlanceVideoSession!
    var videoKey : String = ""
    var glanceAddress : String = ""
    var screenshareStarted : Bool = false
    var showButtons : [NSButton] = [];
    var videoButtons : [NSButton] = [];

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        showButtons.append(self.showMainDisplayButton);
        showButtons.append(self.showSafariButton);
        showButtons.append(self.showThisAppButton);
        
        videoButtons.append(self.showIPhoneButton)
        videoButtons.append(self.showHDButton)
        videoButtons.append(self.showBlackmagicButton)

        settings = GlanceSettings()
        self.username.stringValue = settings.get("GlanceAddress")
        
        GlanceVideoSession.allowScreenCaptureDevices()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    internal func applicationShouldTerminateAfterLastWindowClosed(_ theApplication: NSApplication) -> Bool {
        return true
    }

    @IBAction func login(_ sender: NSButton) {
        if (user == nil) {
            user = GlanceUser()
            user.onEventNotifyTarget(self, selector: #selector(glanceUserEvent))
        }
        // For SSO you can use user.authenticate(withPartnerId:, userId:, key:)
        // using a login key generated by a service you've implemented. See https://help.glance.net/docs/glance-cobrowse/glance-cobrowse-customizing/login-key-for-single-sign-on/
        user.authenticate(withUsername: self.username.stringValue, password: self.password.stringValue)
    }
    
    @IBAction func showMainDisplay(_ sender: NSButton) {
        let dp = GlanceDisplayParams()
        dp!.displayName = "Main"
        _show(dp!, sender: sender);
    }
    
    @IBAction func showSafari(_ sender: NSButton) {
        let app = NSRunningApplication.runningApplications(withBundleIdentifier: "com.apple.Safari").first
        if (app == nil) {
            self.statusText.stringValue = "Safari is not running";
            return;
        }
        let dp = GlanceDisplayParams()
        dp!.displayName = "Process"
        //dp!.process = app.processIdentifier
        dp!.setValue(app!.processIdentifier, forKey: "process");
        _show(dp!, sender: sender);
    }
    
    @IBAction func showThisApp(_ sender: NSButton) {
        let dp = GlanceDisplayParams()
        dp!.displayName = "Process"
        //dp!.process = app.processIdentifier
        dp!.setValue(NSRunningApplication.current.processIdentifier, forKey: "process");
        _show(dp!, sender: sender);
    }
    
    func _show(_ dp : GlanceDisplayParams, sender: NSButton)
    {
        if (screenshareSession == nil) {
            screenshareSession = GlanceHostSession(user: user);
            screenshareSession.onEventNotifyTarget(self, selector: #selector(glanceSessionEvent))
        }
        
        if (screenshareStarted) {
            screenshareSession.showDisplay(dp);
        } else {
            let sp = GlanceStartParams()
            sp!.key = "1234"
            sp!.displayParams = dp;
            screenshareSession.show(sp);
        }
        for button in self.showButtons {
            button.isEnabled = (button != sender)
        }
    }

    @IBAction func endSession(_ sender: NSButton) {
        guard screenshareSession != nil else {
            return;
        }
        screenshareSession.end();
    }
    
    func _sessionStarted(_ sessionKey : String) {
        DispatchQueue.main.async {
            self.screenshareStarted = true;
            self.statusText.stringValue = "Session started, send this link to your guest: https://\(self.settings.get("GlanceServer") ?? "www.glance.net")?username=\(self.glanceAddress)&key=\(sessionKey)"
            //self.startSessionButton.isHidden = true
            self.endSessionButton.isEnabled = true
        }
    }
    
    func _sessionEnded() {
        DispatchQueue.main.async {
            self.screenshareStarted = false;
            self.screenshareSession = nil;
            self.statusText.stringValue = "Session ended";
            self.endSessionButton.isEnabled = false
            for button in self.showButtons {
                button.isEnabled = true
            }
        }
    }

    @objc func glanceUserEvent(_ event: GlanceEvent!) {
        switch event.code {
        case EventLoginSucceeded:
            DispatchQueue.main.async {
                self.statusText.stringValue = "Login succeeded";
                self.settings.set("GlanceAddress", value: self.username.stringValue)
                self.glanceAddress = self.user.getAccountSetting("URL")
                if (self.glanceAddress.isEmpty) {
                    self.glanceAddress = self.user.getAccountSetting("GlanceAddress")
                }
                if (self.glanceAddress.isEmpty) {
                    self.glanceAddress = self.username.stringValue
                }
                self.loginView.isHidden = true;
                self.buttonView.isHidden = false;
            }
            break
        case EventLoginFailed:
            DispatchQueue.main.async {
                self.statusText.stringValue = "Login failed: \(event.message ?? "nil")"
            }
            break
        default:
            // Best practice is to log all other events of type EventError, EventWarning, or EventAssertFail
            print("Glance Event \(event.code) : \(event.message ?? "nil")");
            break
        }
    }
    
    @objc func glanceSessionEvent(_ event: GlanceEvent!) {
        switch event.code {
        case EventConnectedToSession:
            _sessionStarted(event.properties?["sessionkey"] as! String);
            break;
        case EventGuestCountChange:
            DispatchQueue.main.async {
                self.statusText.stringValue = "Guest connected";
            }
            break;
        case EventSessionEnded:
            _sessionEnded();
            break;
        default:
            _showStatus("Glance Event \(event.code) : \(event.message ?? "nil")")
            break;
        }
    }
    
    @IBAction func showiPhone(_ sender: NSButton) {
        _showVideo("/iPhone/", 0, 0, sender: sender)
    }
    
    @IBAction func showHDWebcam(_ sender: NSButton) {
        _showVideo("/HD/", 640, 480, sender: sender)
    }
    
    @IBAction func showBlackmagic(_ sender: NSButton) {
        _showVideo("/Blackmagic/", 720, 576, sender: sender)
    }
    
    func _showVideo(_ deviceName: String, _ width: Int32, _ height: Int32, sender: NSButton)
    {
        if (videoSession == nil) {
            videoSession = GlanceVideoSession(user: user, delegate:self)
            let vserver = user.getAccountSetting("VServer")
            if (!(vserver ?? "").isEmpty) {
                GlanceVideoSession.setServer("https://" + vserver!)
            }
        }
        
        let sp = GlanceStartParams()
        self.videoKey = "4567"
        sp!.key = self.videoKey
        sp!.displayParams.displayName = deviceName
        //sp!.displayParams.captureWidth = width;
        //sp!.displayParams.captureHeight = height;
        sp!.displayParams.setValue(width, forKey: "captureWidth")
        sp!.displayParams.setValue(height, forKey: "captureHeight")
        
        if (videoSession.isStarted()) {
            videoSession.showDisplay(sp!.displayParams);
        } else {
            videoSession.start(sp!);  // this actually works when started too
        }
        
        for button in self.videoButtons {
            button.isEnabled = (button != sender)
        }
    }
    
    @IBAction func endVideo(_ sender: NSButton) {
        guard videoSession != nil else {
            return;
        }
        
        videoSession.end();
    }
    
    func _videoStarted()
    {
        DispatchQueue.main.async {
            self.statusText.stringValue = "Session started, send this link to your guest: https://\(self.settings.get("GlanceServer") ?? "www.glance.net")?username=\(self.glanceAddress)&key=\(self.videoKey)"
            self.endVideoButton.isEnabled = true;
        }
    }
    
    func _videoEnded()
    {
        DispatchQueue.main.async {
            self.videoSession = nil;
            self.statusText.stringValue = "Video ended";
            self.endVideoButton.isEnabled = false;
            for button in self.videoButtons {
                button.isEnabled = true
            }
        }
    }
    
    func _showStatus(_ msg: String)
    {
        DispatchQueue.main.async { self.statusText.stringValue = msg; }
    }
    
    func glanceVideoSessionDidConnectVideoSource(_ session: GlanceVideoSession) {
        _showStatus("Video source connected")
    }
    
    func glanceVideoSessionDidFailToConnectVideoSource(_ session: GlanceVideoSession, error: Error) {
        _showStatus("Video failed to connect source: \(error.localizedDescription)")
    }
    
    func glanceVideoSessionDidDisconnectVideoSource(_ session: GlanceVideoSession) {
        _showStatus("Video source disconnected")
    }
    
    func glanceVideoSessionDidStartVideoCapture(_ session: GlanceVideoSession) {
        _showStatus("Video capture started")
    }
    
    func glanceVideoSessionDidConnectStreamer(_ session: GlanceVideoSession) {
        _showStatus("Video streamer connected")
    }
    
    func glanceVideoSessionDidFailConnectStreamer(_ session: GlanceVideoSession, error: Error) {
        _showStatus("Video failed to connect streamer: \(error.localizedDescription)")
    }
    
    func glanceVideoSessionDidDisconnnectStreamer(_ session: GlanceVideoSession) {
        _showStatus("Video streamer disconnected")
    }
    
    func glanceVideoSessionWillStartStreaming(_ session: GlanceVideoSession) {
        _showStatus("Guest connected to video")
    }
    
    func glanceVideoSessionWillStopStreaming(_ session: GlanceVideoSession) {
        _showStatus("Guest disconnected from video")
    }
    
    func glanceVideoSessionWillChangeQuality(_ session: GlanceVideoSession) {
        _showStatus("Video quality changed")
    }
    
    func glanceVideoSessionDidStart(_ session: GlanceVideoSession) {
        _videoStarted();
    }
    
    func glanceVideoSessionDidEnd(_ session: GlanceVideoSession) {
        _videoEnded();
    }
}

