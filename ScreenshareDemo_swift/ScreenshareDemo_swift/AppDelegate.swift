//
//  AppDelegate.swift
//  ScreenshareDemo_swift
//
//  Created by Ankit Desai on 8/6/19.
//  Copyright © 2019 Glance Networks. All rights reserved.
//

import Cocoa
import GlanceFramework

let GLANCE_GROUP_ID : Int32 = 15687

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, GlanceVisitorDelegate {
    @IBOutlet weak var startSessionButton: NSButton!
    @IBOutlet weak var endSessionButton : NSButton!
    @IBOutlet weak var sessionKeyLabel : NSTextField!
    @IBOutlet weak var maskedField: NSTextField!
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        // Make sure the GlanceVisitor is listening for events on this ViewController
        GlanceVisitor.add(self)
        
        GlanceVisitor.initVisitor(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "")
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
        GlanceVisitor.endSession()
    }
    
    internal func applicationShouldTerminateAfterLastWindowClosed(_ theApplication: NSApplication) -> Bool {
        return true
    }

    @IBAction func startSession(_ sender: NSButton) {
        GlanceVisitor.setMaskedViews([maskedField])
        GlanceVisitor.startSession()
    }
    
    @IBAction func endSession(_ sender: NSButton) {
        GlanceVisitor.endSession()
        GlanceVisitor.setMaskedViews([])
    }
    
    func _sessionStarted(_ sessionKey : String) {
        DispatchQueue.main.async {
            self.sessionKeyLabel.stringValue = sessionKey
            self.startSessionButton.isHidden = true
            self.endSessionButton.isHidden = false
        }
    }
    
    func _sessionEnded() {
        DispatchQueue.main.async {
            self.endSessionButton.isHidden = true
            self.startSessionButton.isHidden = false
        }
    }

    func glanceVisitorEvent(_ event: GlanceEvent!) {
        switch event.code {
        case EventConnectedToSession:
            _sessionStarted(event.properties["sessionkey"] as! String)
            break
        case EventSessionEnded:
            _sessionEnded()
            break
        default:
            // Best practice is to log all other events of type EventError, EventWarning, or EventAssertFail
            print("Glance Event \(event.code) : \(event.message ?? "nil")");
            break
        }
    }
}

